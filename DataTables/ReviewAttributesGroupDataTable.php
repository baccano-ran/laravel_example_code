<?php

namespace App\DataTables;

use App\Repositories\Eloquent\Models\AttributeGroup;
use App\Repositories\Eloquent\Models\ReviewType;

/**
 * Class ReviewAttributesGroupDataTable
 * @package App\DataTables
 */
class ReviewAttributesGroupDataTable extends AdminDataTable
{
    /**
     * @inheritdoc
     */
    public function dataTable($query)
    {
        return datatables($query)
           ->addColumn(
               'action',
               function (AttributeGroup $attributesGroup) {
                   return view(
                       'admin.datatables.columns.actions',
                       [
                           'entityId' => $attributesGroup->id,
                           'entityName' => 'review-attributes-group'
                       ]
                   );
               }
            )->addColumn(
                'created_at',
                function ($attributesGroup) {
                    return view('admin.datatables.columns.time', ['time' => $attributesGroup->created_at]);
                }
            )->addColumn(
                'updated_at',
                function ($attributesGroup) {
                    return view('admin.datatables.columns.time', ['time' => $attributesGroup->updated_at]);
                }
            )->rawColumns(['action', 'created_at', 'updated_at'])
            ->orderColumn('created_at', 'created_at $1')
            ->orderColumn('updated_at', 'updated_at $1');
    }

    /**
     * Get query source of dataTable.
     *
     * @param AttributeGroup $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(AttributeGroup $model)
    {
        return $model->newQuery()
            ->leftJoin('entity_type_group', function ($join) {
                $join
                    ->on(AttributeGroup::getTableName() . '.id', '=', 'entity_type_group.group_id')
                    ->where('entity_type_group.entity_type', '=', ReviewType::class)
                    ->orWhere('entity_type_group.entity_type', '=', null);
            })
            ->leftJoin('review_attribute_group', 'review_attribute_group.group_id', '=', 'attribute_group.id')
            ->leftJoin('review_types', 'review_types.id', '=', 'review_attribute_group.entity_type')
            ->leftJoin('attributes', 'attributes.id', '=', 'review_attribute_group.attribute_id')
            ->groupBy('attribute_group.id')
            ->select(
                \DB::raw('COUNT(attributes.id) as attributes_count'),
                AttributeGroup::getTableName() . '.title as title',
                AttributeGroup::getTableName() . '.id as id',
                AttributeGroup::getTableName() . '.created_at',
                AttributeGroup::getTableName() . '.updated_at',
                ReviewType::getTableName() . '.name as type_name',
                'entity_type_group.group_id as group_id'
            );
    }

    /**
     * @inheritdoc
     */
    protected function getBuilderParameters()
    {
        $params = parent::getBuilderParameters();
        $params['order'] = [
            [0, 'asc'],
        ];

        return $params;
    }

    /**
     * @inheritdoc
     */
    protected function getColumns()
    {
        return [
            [
                'name' => AttributeGroup::getTableName() . '.title',
                'data' => 'title',
                'title' => __('admin.title'),
                'searchable' => true
            ],
            [
                'name' => 'attributes_count',
                'data' => 'attributes_count',
                'searchable' => false,
                'title' => __('admin.attributes_count')
            ],
            [
                'name' => ReviewType::getTableName() . '.name',
                'data' => 'type_name',
                'orderable' => false,
                'searchable' => true,
                'title' => __('admin.related_review_type_name')
            ],
            [
                'name' => 'created_at',
                'data' => 'created_at',
                'title' => __('admin.created_at'),
                'searchable' => false
            ],
            [
                'name' => 'updated_at',
                'data' => 'updated_at',
                'title' => __('admin.updated_at'),
                'searchable' => false
            ],
        ];
    }
}
