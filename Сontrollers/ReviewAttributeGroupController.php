<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ReviewAttributesGroupDataTable;
use App\Helpers\BreadcrumbsHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAttributeGroup;
use App\Repositories\Eloquent\Enums\HTTPStatusCode;
use App\Repositories\Eloquent\Models\AttributeGroup;
use App\Repositories\Eloquent\Models\ReviewType;
use App\Repositories\Eloquent\ReviewAttributesGroupRepository;
use App\Services\AttributeGroupService;

class ReviewAttributeGroupController extends Controller
{
    /**
     * Current page breadcrumbs
     *
     * @var array
     */
    protected $breadcrumbs = [
        [
            'name' => 'Reviews attribute group',
            'route' => 'review-attributes-group.index'
        ]
    ];

    /**
     * @var ReviewAttributesGroupRepository
     */
    protected $reviewAttributesGroupRepository;

    public function __construct(ReviewAttributesGroupRepository $reviewAttributesGroupRepository)
    {
        $this->reviewAttributesGroupRepository = $reviewAttributesGroupRepository;

        $this->middleware('permission:read-reviewAttributeGroup')->only('index', 'show');
        $this->middleware('permission:create-reviewAttributeGroup')->only('create', 'store');
        $this->middleware('permission:update-reviewAttributeGroup')->only('edit', 'update');
        $this->middleware('permission:delete-reviewAttributeGroup')->only('destroy');
    }

    /**
     * Display a listing of the reviews types.
     *
     * @param ReviewAttributesGroupDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(ReviewAttributesGroupDataTable $dataTable)
    {
        return $dataTable->render('admin.datatables.table', [
            'breadcrumbs' => $this->breadcrumbs,
            'entityName' => AttributeGroup::getTableName()
        ]);
    }

    /**
     * Show the form for creating a new review type.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        BreadcrumbsHelper::createBreadcrumb($this->breadcrumbs, 'Add new attributes group');

        $this->appendAppEntryPoint('reviewAttributesGroup.create');

        return view('admin.review-group-attributes.create', [
            'attributesGroup' => new AttributeGroup(),
            'breadcrumbs' => $this->breadcrumbs
        ]);
    }

    /**
     * Store a newly created review in database.
     * @param  StoreAttributeGroup $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAttributeGroup $request)
    {
        $attributes = (array) $request->get('attribute');
        $reviewType = (string) $request->get('review_type');

        $type = $this->reviewAttributesGroupRepository->create([
            'title' => (string) $request->get('title'),
        ]);
        $attributesGroupId = $type->id;

        if (!empty($attributes) && $attributesGroupId) {
            $attributeGroupService = new AttributeGroupService();
            $attributeGroupService->saveAttributes($attributes, $reviewType, $attributesGroupId);
        }

        return redirect()
            ->route('review-attributes-group.index')
            ->with('success-msg', 'Review group successfully added.');
    }

    /**
     * Show the form for editing the specified review type.
     *
     * @param int $attributesGroupId
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function edit($attributesGroupId)
    {
        $attributesGroup = $this->reviewAttributesGroupRepository->find((int) $attributesGroupId);

        if (!$attributesGroup) {
            abort(404);
        }

        BreadcrumbsHelper::createBreadcrumb($this->breadcrumbs, 'Edit attributes group');

        $this->appendAppEntryPoint('reviewAttributesGroup.create');

        return view('admin.review-group-attributes.edit', [
            'breadcrumbs' => $this->breadcrumbs,
            'attributesGroup' => $attributesGroup
        ]);
    }

    /**
     * Update the specified review in database.
     *
     * @param  StoreAttributeGroup $request
     * @param  int $attributesGroupId
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function update(StoreAttributeGroup $request, $attributesGroupId)
    {
        $attributesGroupId = (int) $attributesGroupId;
        $updateDta = $request->validated();
        $this->reviewAttributesGroupRepository->update($updateDta, $attributesGroupId);

        $attributes = $request->get('attribute');
        $entityType = $request->get('review_type');

        if (!empty($attributes) && $attributesGroupId) {
            $attributeGroupService = new AttributeGroupService();
            try {
                $attributeGroupService->saveAttributes($attributes, $entityType, $attributesGroupId, true);
            } catch (\Exception $e) {
                return \Response::json([
                    'message' => $e->getMessage(),
                ], HTTPStatusCode::UNPROCESSIBLE_ENTITY);
            }
        }

        return response()->json(['message' => 'Entity update successful!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $attributesGroupId
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($attributesGroupId)
    {
        $attributesGroupId = (int) $attributesGroupId;
        $attributesGroup = $this->reviewAttributesGroupRepository->find($attributesGroupId);

        if ($attributesGroup->reviewTypes->count() > 0) {
            return response()->json(['error' => 'Can\'t delete group, it\'s used in existing review types!']);
        }

        if ($this->reviewAttributesGroupRepository->delete($attributesGroupId)) {
            return response()->json(['success' => 'Review group successfully deleted']);
        }
        return response()->json(['error' => 'An error occured while deleting record']);
    }

    public function getAttributesById($reviewTypeId, $groupId)
    {
        $attributeGroupService = new AttributeGroupService();
        $attributes = $attributeGroupService->getAttributesByTypeId((int) $reviewTypeId, ReviewType::class);
        $oldAttributes = $attributeGroupService->getOldAttributesByGroupId((int) $groupId);

        return response()->json([
            'attributes' => $attributes,
            'oldAttributes' => $oldAttributes,
        ]);
    }
}
