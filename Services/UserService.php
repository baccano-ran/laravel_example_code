<?php

namespace App\Services;

use App\Repositories\Eloquent\Enums\{
    UserStatus,
    VisibilityStatus
};
use App\Repositories\Eloquent\Models\{
    Language,
    User
};
use App\Repositories\Exceptions\DuplicateException;
use App\{
    Role,
    Team
};
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserService extends AbstractService
{
    /**
     * @param string $teamName
     * @return \App\Team|null
     * @codeCoverageIgnore
     */
    public function getTeamByName(string $teamName)
    {
        return Team::getByName($teamName);
    }

    /**
     * @param string $roleName
     * @return \App\Role|null
     * @codeCoverageIgnore
     */
    public function getRoleByName(string $roleName)
    {
        return Role::getByName($roleName);
    }

    /**
     * @param User $user
     * @param string $teamName
     * @param string $roleName
     * @param bool $attach
     * @return Team
     * @throws DuplicateException
     */
    public function setTeam(User $user, string $teamName, string $roleName, bool $attach = true): Team
    {
        $userId = (int) $user->id;
        $exists = $this->checkTeamExist($teamName, $user);

        if ($exists && $attach) {
            throw new DuplicateException('Team already exists');
        }

        if (!$exists && !$attach) {
            throw new ModelNotFoundException('Team with name: "' . $teamName . '" not found.');
        }

        if (!$user->id) {
            throw new ModelNotFoundException('User with id: "' . $userId . '" not found.');
        }

        $team = $this->getTeamByName($teamName);

        if ($team === null) {
            throw new ModelNotFoundException('Team with name: "' . $teamName . '" not found.');
        }

        $writer = $this->getRoleByName($roleName);

        if ($writer === null) {
            throw new ModelNotFoundException('Role with name: "' . $roleName . '" not found.');
        }

        if ($attach) {
            $user->attachRole($writer, $team);
        } else {
            $user->detachRole($writer, $team);
        }

        return $team;
    }

    /**
     * @param string $teamName
     * @param User $user
     * @return bool
     * @codeCoverageIgnore
     */
    public function checkTeamExist(string $teamName, User $user): bool
    {
        return in_array($teamName, $user->rolesTeams()->pluck('name')->all());
    }

    /**
     * @return array
     * @codeCoverageIgnore
     */
    public function getAvailableTeams()
    {
        return Team::getTeamsWithNamePrefix();
    }

    /**
     * @param User $user
     * @return array
     * @codeCoverageIgnore
     */
    public function getRoleNames(User $user)
    {
        return $user->roles->pluck('name')->all();
    }

    /**
     * @param User $user
     * @return array
     * @codeCoverageIgnore
     */
    public function getTeamNames(User $user)
    {
        return $user->rolesTeams->pluck('name')->all();
    }

    /**
     * @param User $user
     * @return bool
     * @codeCoverageIgnore
     */
    public function hasRoleWriter(User $user): bool
    {
        $roleNames = $this->getRoleNames($user);
        return in_array(Role::WRITER, $roleNames);
    }

    /**
     * @param User $user
     * @return bool
     * @codeCoverageIgnore
     */
    public function hasLanguageTeamFitCurrentLocal(User $user): bool
    {
        $currentTeamName = Team::LANGUAGE_PREFIX . \App::getLocale();
        return in_array($currentTeamName, $this->getTeamNames($user));
    }

    /**
     * @param User $user
     * @return array
     */
    public function getTeamsLanguagesIds(User $user): array
    {
        $teams = $this->getTeamNames($user);
        $teamsIsoCodes = array_map(function ($singleTeam) {
            return explode('_', $singleTeam)[1] ?? [];
        }, $teams);

        return Language::getActiveLanguages()->whereIn('iso_code', $teamsIsoCodes)->pluck('id')->toArray();
    }

    /**
     * @param User $user
     * @param string $role
     * @param null $team
     * @param bool $requireAll
     * @return bool
     */
    public function hasRole($user, $role, $team = null, $requireAll = false)
    {
        $hasRoleWriter = $this->hasRoleWriter($user);
        $hasTeam = $this->hasLanguageTeamFitCurrentLocal($user);

        if ($hasRoleWriter && !$hasTeam) {
            return false;
        }

        return $user->laratrustHasRole($role, $team, $requireAll);
    }

    /**
     * @return array
     */
    public static function getProfileVisibilityStatuses()
    {
        return [
            VisibilityStatus::SHOW => __('account.status.public'),
            VisibilityStatus::SHOW_ONLY_AUTHORIZED => __('account.status.only_for_authorized'),
            VisibilityStatus::HIDE => __('account.status.hidden'),
        ];
    }

    /**
     * @param int $id
     */
    public static function updateAccountStatusById($id)
    {
        User::query()->where('id', $id)->update(['status' => UserStatus::ACTIVE, 'registration_token' => null]);
    }
}
