<?php

namespace App\Services;

use App\Repositories\Eloquent\Models\AttributeGroup;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class AttributeGroupService
{
    /**
     * @param array $groups
     * @param int $entityId
     * @param string $entityType
     * @param bool $update
     * @return bool
     */
    public function saveGroup(array $groups, int $entityId, string $entityType, bool $update = false): bool
    {
        if ($update) {
            $delete = $this->deleteOldGroups($entityId, $entityType);
            if (!$delete) {
                throw new ModelNotFoundException('Group not found.');
            }
        }

        $groups = $this->formatGroupDataBeforeSave($groups, $entityId, $entityType);
        return DB::table('entity_type_group')->insert($groups);
    }

    /**
     * @param array $groups
     * @param int $entityId
     * @param string $entityType
     * @return array
     */
    protected function formatGroupDataBeforeSave(array $groups, int $entityId, string $entityType): array
    {
        $formattedGroups = [];
        foreach ($groups as $key => $groupId) {
            $insert = [
                'group_id' => $groupId,
                'entity_id' => $entityId,
                'entity_type' => $entityType
            ];
            $formattedGroups[] = $insert;
        }
        return $formattedGroups;
    }

    /**
     * @param int $entityId
     * @param string $entityType
     * @return bool
     */
    private function deleteOldGroups(int $entityId, string $entityType): bool
    {
        $entity = $entityType::find($entityId);

        if ($entity) {
            $oldGroups = $entity->groups->pluck('id')->all();
            DB::table('entity_type_group')
                ->where('entity_id', '=', $entityId)
                ->where('entity_type', '=', $entityType)
                ->whereIn('group_id', $oldGroups)->delete();
            return true;
        }

        return false;
    }

    /**
     * @param array $attributes
     * @param int $reviewType
     * @param int $groupId
     * @param bool $update
     * @return bool
     */
    public function saveAttributes(array $attributes, int $reviewType, int $groupId, bool $update = false): bool
    {
        if ($update) {
            $delete = $this->deleteOldAttributes($groupId);
            if (!$delete) {
                throw new ModelNotFoundException('Attribute not found.');
            }
        }
        $attributes = $this->formatAttributeDataBeforeSave($attributes, $reviewType, $groupId);

        return DB::table('review_attribute_group')->insert($attributes);
    }

    /**
     * @param array $attributes
     * @param int $reviewType
     * @param int $groupId
     * @return array
     */
    private function formatAttributeDataBeforeSave(array $attributes, int $reviewType, int $groupId): array
    {
        $formattedGroups = [];
        foreach ($attributes as $key => $attribute) {
            $insert = [
                'group_id' => $groupId,
                'attribute_id' => $attribute,
                'entity_type' => $reviewType,
            ];
            $formattedGroups[] = $insert;
        }
        return $formattedGroups;
    }

    /**
     * @param int $entityId
     * @return bool
     */
    private function deleteOldAttributes(int $entityId): bool
    {
        $attributeGroup = AttributeGroup::find($entityId);

        if ($attributeGroup) {
            $oldGroups = $attributeGroup->attributes->pluck('id')->all();
            DB::table('review_attribute_group')
                ->where('group_id', '=', $entityId)
                ->whereIn('attribute_id', $oldGroups)->delete();
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public static function getTypesOptions(): array
    {
        return AttributeGroup::getReviewAttributesGroup()
            ->pluck('title', 'title')
            ->all();
    }

    /**
     * @param int $reviewTypeId
     * @param $entityType
     * @return array
     */
    public function getAttributesByTypeId(int $reviewTypeId, $entityType)
    {
        $entity = $entityType::find($reviewTypeId);
        $attributes = $entity->getReviewTypeAttributes;
        $groupAttributes = $entity->getAttributesInGroup;

        if ($attributes->isNotEmpty()) {
            $attributes = $attributes->sortBy('title')->pluck('id', 'title')->all();
        }

        if ($groupAttributes->isNotEmpty()) {
            $groupAttributes = $groupAttributes->sortBy('title')->pluck('id', 'title')->all();
            $attributes = $this->filterAttributes($attributes, $groupAttributes);
        }

        return $attributes;
    }

    /**
     * @param array $attributes
     * @param array $groupAttributes
     * @return array
     */
    private function filterAttributes(array $attributes, array $groupAttributes): array
    {
        return array_diff($attributes, $groupAttributes);
    }

    /**
     * @param int $groupId
     * @return \App\Repositories\Eloquent\Models\Attribute[]|array|\Illuminate\Database\Eloquent\Collection
     */
    public function getOldAttributesByGroupId(int $groupId)
    {
        if ($groupId === 'new') {
            return  $attributes = [];
        }

        $group = AttributeGroup::find($groupId);
        $attributes = $group->attributes;

        return $attributes->isNotEmpty() ? $attributes->pluck('title', 'id')->all() : [];
    }
}
