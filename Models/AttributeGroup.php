<?php

namespace App\Repositories\Eloquent\Models;

use App\Repositories\Eloquent\Traits\EloquentEntityNameTrait;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Repositories\Eloquent\Models\AttributeGroup
 *
 * @property int $id
 * @property string $title
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Repositories\Eloquent\Models\ReviewType[] $reviewTypes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Repositories\Eloquent\Models\PostCategory[] $postCategories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Repositories\Eloquent\Models\Attribute[] $attributes
 * @method static \Illuminate\Database\Eloquent\Builder|AttributeGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AttributeGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AttributeGroup whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AttributeGroup whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AttributeGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Repositories\Eloquent\Models\Review[] $reviews
 */
class AttributeGroup extends Model
{
    use EloquentEntityNameTrait, Eloquence;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * Table name which is associated with this model
     *
     * @var string
     */
    public $table = 'attribute_group';

    /**
     * @param $entityType
     * @return array
     */
    public static function getGroupsByEntityTypeId($entityType): array
    {
        return self::query()
            ->join('review_attribute_group', 'attribute_group.id', '=', 'review_attribute_group.group_id')
            ->where('review_attribute_group.entity_type', '=', $entityType)
            ->groupBy('review_attribute_group.group_id')
            ->select('attribute_group.id as id', 'attribute_group.title as title')
            ->get()
            ->all();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function reviewTypes()
    {
        return $this->morphedByMany(ReviewType::class, 'entity', 'entity_type_group', 'group_id', 'entity_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function postCategories()
    {
        return $this->morphedByMany(PostCategory::class, 'entity', 'entity_type_group', 'group_id', 'entity_id');
    }

    public function attributes()
    {
        return $this
           ->belongsToMany(Attribute::class, 'review_attribute_group', 'group_id', 'attribute_id')
           ->withPivot('entity_type');
    }

    /**
     * @param $reviewType
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function reviewAttributes($reviewType)
    {
        return $this
            ->attributes()
            ->wherePivot('entity_type', '=', $reviewType)
            ->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reviews()
    {
        return $this->belongsToMany(Review::class, 'review_attribute_group', 'group_id', 'review_id');
    }

    /**
     * @return $this
     */
    public static function getReviewAttributesGroup()
    {
        return self::query()
            ->leftJoin('entity_type_group', 'attribute_group.id', '=', 'entity_type_group.group_id')
            ->distinct('attribute_group.id')
            ->where('entity_type_group.entity_type', '=', ReviewType::class)
            ->orWhere('entity_type_group.entity_type', '=', null)
            ->select('title', 'slug', 'attribute_group.id', 'entity_type_group.group_id');
    }
}
