<?php

namespace App\Repositories\Eloquent\Models;

use App\Repositories\Eloquent\Traits\EloquentEntityNameTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

/**
 * App\Repositories\Eloquent\Models\ReviewType
 *
 * @property int $id
 * @property string $name
 * @property string $singular_name
 * @property string $description
 * @property string $description_title
 * @property string $slug
 * @property int $order
 * @property int $show_in_menu
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Repositories\Eloquent\Models\AttributeGroup[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Repositories\Eloquent\Models\Review[] $reviews
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Repositories\Eloquent\Models\ReviewTypeTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType ordered($direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType whereShowInMenu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\Eloquent\Models\ReviewType withTranslation()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Repositories\Eloquent\Models\AttributeReview[] $attributes
 */
class ReviewType extends Model implements Sortable
{
    use EloquentEntityNameTrait, SortableTrait, Translatable;

    const IS_CHOOSABLE = 1;
    const IS_NOT_CHOOSABLE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'singular_name', 'description', 'description_title', 'slug', 'is_choosable'];

    public $sortable = [
        'order_column_name' => 'order'
    ];

    public $translatedAttributes = ['translated_title', 'translated_singular_title', 'translated_description', 'translated_description_title'];

    /**
     * Disable timestamps saving to database
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Set model relationships
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany('App\Repositories\Eloquent\Models\Review', 'type_id', 'id');
    }

    public function groups()
    {
        return $this->morphToMany(AttributeGroup::class, 'entity', 'entity_type_group', 'entity_id', 'group_id');
    }

    /**
     * Get all attributes of review type
     */
    public function attributes()
    {
        return $this->hasManyThrough(AttributeReview::class, Review::class, 'type_id', 'review_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getReviewTypeAttributes()
    {
        return $this->belongsToMany(Attribute::class, 'review_type_attribute', 'review_type_id', 'attribute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getAttributesInGroup()
    {
        return $this->belongsToMany(Attribute::class, 'review_attribute_group', 'entity_type', 'attribute_id');
    }

    /**
     * @return $this
     */
    public function translateTitle()
    {
        return $this->hasOne(ReviewTypeTranslation::class)->where('locale', '=', \App::getLocale());
    }

    /**
     * @return Collection
     */
    public static function getShowInMenuReviewTypes(): Collection
    {
        return self::where('show_in_menu', true)->with('translateTitle')->orderBy('name')->get();
    }

    /**
     * @param string $reviewTypeSlug
     * @return bool
     */
    public static function hasByCountryBehaviourBySlug(string $reviewTypeSlug)
    {
        return in_array($reviewTypeSlug, self::getByCountryReviewTypes());
    }

    /**
     * @return array
     */
    public static function getByCountryReviewTypes()
    {
        return [
            Review::SLUG_EXCHANGES,
            Review::SLUG_GAMBLING,
            Review::SLUG_FOREX_BROKERS,
        ];
    }
}
